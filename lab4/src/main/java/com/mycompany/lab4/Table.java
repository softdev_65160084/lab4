/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab4;

/**
 *
 * @author Siwak
 */
public class Table{
    private char[][] table={{'-','-','-'},
                            {'-','-','-'},
                            {'-','-','-'}};
    private Player player1,player2,currentPlayer;
    private int count=0;
    private int row,col;

    public Table(Player player1 ,Player player2 ){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public char[][] getTable() {
        return table;
    }
    
    public Player getCurrentPlayer() {
        return currentPlayer;
    }
    
    public boolean setRowCol(int row,int col){
        if(table[row-1][col-1]=='-'){
            table[row-1][col-1] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            count++;
            return true;
        }
        return false;
    }
    public void switchPlayer(){
        if(currentPlayer==player1){
            currentPlayer = player2;
        }else{
            currentPlayer = player1;
        }
    }
    public boolean checkWin(){
        if(checkRow()){
            saveWin();
            return true;
        }
        if(checkCol()){
            saveWin();
            return true;
        }
        return false;
    }
    
    public boolean checkRow(){
        return table[row-1][0] != '-' && table[row-1][0] == table[row-1][1] && table[row-1][1] == table[row-1][2];
    }
    
    public boolean checkCol(){
        return table[0][col-1] != '-' && table[0][col-1] == table[1][col-1] && table[1][col-1] == table[2][col-1];
    }
    
    public boolean checkDraw(){
        if(count==9){
            saveDraw();
            return true;
        }
        return false;
    }
    
    public void saveWin(){
        if(player1 == currentPlayer){
            player1.win();
            player2.lose();
        }else{
            player2.win();
            player1.lose();
        }
    }
    
    public void saveDraw(){
        player1.draw();
        player2.draw();
    }
}
